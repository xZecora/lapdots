#!/bin/sh

# Import the colors
BG="#282828"
FG="#EBDBB2"
ORANGE="#D65D0E"
RED="#CC241D"
GREEN="#98971A"
YELLOW="#D79921"
BLUE="#83A598"
PURPLE="#B16286"
AQUA="#689D6A"

dmenu_run -nb "$BG" -nf "$FG" -sb "$AQUA" -sf "$FG" -shb "$YELLOW" -shf "$ORANGE" -nhf "$ORANGE" -nhb "$BG" -fn "MesloLGS NF:pixelsize=18" -l 9
