#!/bin/sh

killall dzen2 
XPOS=0
WIDTH=1920
HEIGHT=24
FONT="monospace-11"
FG="#EBDBB2"
BG="#282828"

GREEN="^fg(#98971A)^bg()"
YELLOW="^fg(#D79921)^bg()"
RED="^fg(#CC241D)^bg()"
WHITE="^fg(#A89984)^bg()"
ORANGE="^fg(#D65D0E)^bg()"
BLUE="^fg(#458588)^bg()"
MAGENTA="^fg(#b16286)^bg()"


CLEAN="^fg()^bg()"

while true; do

	XWINDOW=$(echo "$ORANGE`echo $(xdotool getwindowfocus getwindowname | grep -o '^.\{25\}')`$CLEAN")
  WMNAME=$(echo "$ORANGE ratpoison $CLEAN")
	TIME=$(echo "$GREEN`echo $(date +"%R %a %d %B")`$CLEAN")
	BATTERYLINE=$(echo "$RED`cat /sys/class/power_supply/BAT1/capacity `%$CLEAN")		
	KERNEL=$(echo "$YELLOW`echo $(uname -r)`$CLEAN")
	UPDATES=$(echo "Updates: $WHITE`echo $(checkupdates | wc -l)`$CLEAN")
	WIFILINE=$(echo "SSID: $BLUE`echo $(iw dev wlo1 info | grep ssid | awk '{$1=""; print}')`$CLEAN")

	echo " $WMNAME |  $BATTERYLINE  |  $TIME  |  $KERNEL  |  $UPDATES  |  $WIFILINE "

	sleep 1	

done | dzen2 -h $HEIGHT -ta l -x $XPOS -w $WIDTH -fg $FG -bg $BG -fn $FONT
