#!/bin/sh

# Import the colors
. "${HOME}/.cache/wal/colors.sh"

dmenu_run -nb "$color0" -nf "$color15" -sb "$color1" -sf "$color15" -shb "$color1" -shf "$color4" -nhf "$color5" -nhb "$color0" -fn "MesloLGS NF:pixelsize=18" -l 9
